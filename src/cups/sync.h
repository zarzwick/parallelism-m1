#ifndef M1_CUPS_SYNC_H
#define M1_CUPS_SYNC_H

#include "conf.h"
#include "../monitor/prodcons.h"

#include <pthread.h>


/**
 * Synchronization material.
 */
typedef struct CUPSSync {

    // Raw jobs to be filtered.
    ProdCons rawJobs;

    // Filtered jobs to be printed (1 by printer).
    ProdCons* jobs;

    // Orders to be send to printers directly (1 by printer).
    ProdCons* orders;
    
} CUPSSync;


/**
 * Create the synchronization structure.
 * 
 * This function will create many shared buffers:
 * . One managing the unprocessed jobs passed from scheduler to filters.
 * . A pair (jobs, orders) by printer.
 * 
 * @return -1: bad, 0: good.
 */
int cupsCreateSync(CUPSSync* sync, CUPSConf* conf);


/**
 * Clean the synchronization structure.
 */
void cupsCleanSync(CUPSSync* sync, CUPSConf* conf);


#endif
