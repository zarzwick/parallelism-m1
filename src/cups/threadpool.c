#include "threadpool.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


/**
 * Create the thread pool and initialize everything.
 */
int cupsInitThreads(CUPSConf* conf,
                    CUPSSync* sync,
                    CUPSThreadPool* pool,
                    CUPSJobList* joblist,
                    int serv)
{
    assert(conf != NULL);
    assert(pool != NULL);
    
    pool->threads       = NULL;
    pool->fltrArgs      = NULL;
    pool->prntrCliArgs  = NULL;
    
    
    // Filter threads and Printer threads. Scheduler is unique.
    pool->filterCount   = conf->filterCount;
    pool->printerCount  = conf->printerCount;

    
    // Register the total count of threads
    pool->threadCount = 1 + pool->filterCount + pool->printerCount;
   
    
    // Allocate storage for every thread
    if ((pool->threads = calloc(pool->threadCount, sizeof(pthread_t))) == NULL)
        return -1;
        

    // Set some indirections
    pool->schedulerThread   = pool->threads;
    pool->filtersThread     = pool->threads + 1;
    pool->printersThread    = pool->threads + 1 + pool->filterCount;
    
    
    // Create arguments (we would all need this in life)
    pool->schedArgs.conf        = conf;
    pool->schedArgs.serv        = serv;
    pool->schedArgs.jobsQueue   = &sync->rawJobs;
    pool->schedArgs.ordsQueue   = sync->orders;
    pool->schedArgs.joblist     = joblist;
    
    pool->prntrCliArgs = malloc(pool->printerCount * sizeof(PrinterCliArgs));
    for (unsigned int i = 0; i < pool->printerCount; ++ i)
    {
        pool->prntrCliArgs[i].p         = &(conf->printers[i]);
        pool->prntrCliArgs[i].jobsQueue = sync->jobs;
        pool->prntrCliArgs[i].ordsQueue = sync->orders;
        pool->prntrCliArgs[i].joblist   = joblist;
    }
    
    pool->fltrArgs = malloc(pool->filterCount * sizeof(FilterArgs));
    for (unsigned int i = 0; i < pool->filterCount; ++ i)
    {
        pool->fltrArgs[i].id        = i;
        pool->fltrArgs[i].jobsQueue = &sync->rawJobs;
        pool->fltrArgs[i].jobsReady = sync->jobs;
        pool->fltrArgs[i].joblist   = joblist;
    }
    
    
    return 0;
}


/**
 * Clean up arguments.
 */
void cupsCleanThreads(CUPSThreadPool* pool)
{
    assert(pool->threads);
    assert(pool->fltrArgs);
    assert(pool->prntrCliArgs);
    
    free(pool->threads);
    free(pool->fltrArgs);
    free(pool->prntrCliArgs);
}


/**
 * Start aaaaaaaaaaaaaaallll threads !!!
 */
int cupsStartThreads(CUPSThreadPool* pool)
{
    int err;
    
    // Create the scheduler thread
    err = pthread_create(pool->schedulerThread, NULL, cupsScheduler, &(pool->schedArgs));
    
    if (err != 0)
        fprintf(stderr, "[thread] Error for sched.\n");

    // Create the filter(s) thread(s)
    for (unsigned int i = 0; i < pool->filterCount; ++ i)
    {
        err = pthread_create(&(pool->filtersThread[i]), NULL, cupsFilter, &(pool->fltrArgs[i]));
        
        if (err != 0)
            fprintf(stderr, "[thread] Error for filt. %d\n", i);
    }
    
    // Create the printer client(s) thread(s)
    for (unsigned int i = 0; i < pool->printerCount; ++ i)
    {
        err = pthread_create(&(pool->printersThread[i]), NULL,
                             cupsPrinterCli, &(pool->prntrCliArgs[i]));
        if (err != 0)
            fprintf(stderr, "[thread] Error for print. %d\n", i);
    }
    
    return 0;
}


/**
 * Join all threads.
 */
int cupsJoinThreads(CUPSThreadPool* pool)
{
    // For every thread
    for (int t = pool->threadCount - 1; t > -1; --t)
    {
        if (pool->threads[t])
        pthread_join(pool->threads[t], NULL);
    }
    
    return 0;
}
