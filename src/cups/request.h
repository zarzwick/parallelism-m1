#ifndef M1_CUPS_REQUEST_H
#define M1_CUPS_REQUEST_H

#include "job.h"
#include "conf.h"
#include "../printer/printertask.h"


/**
 * Request types.
 */
typedef enum CUPSReqType {
    REQ_SUBMIT = 0,
    REQ_CANCEL,
    REQ_JOB_STATUS,
    REQ_PRINTER_STATUS,
    REQ_COUNT
} CUPSReqType;


/**
 * Simple structure representing the possible requests to the CUPS server.
 * Values are optionnal, depending on the request.
 * 
 * @note Undemanded values won't be read and can be left undefined.
 */
typedef struct CUPSRequest {
    
    CUPSReqType reqType;    // Request type (mandatory)

    union {

        // Submit type
        struct {
            MimeType        mimeType;
            unsigned int    nbCopies;
            bool            rectoVerso;
            char            printerName[M1_PRINTER_NAMELEN];
        } submit;

        // Cancel type
        struct {
            unsigned int id;
        } cancel;

        // Job status type
        struct {
            unsigned int id;
        } jobStatus;

        // Printer status type
        struct {
            char printerName[M1_PRINTER_NAMELEN];
        } printerStatus;

    };

} CUPSRequest;


/**
 * The return type.
 */
typedef struct CUPSReturn {
    
    union {
        struct {
            unsigned int    id;
        } submitted;
        
        struct {
            PrinterStatus   status;
            unsigned int    currentJobID;
        } printerStatus;
        
        struct {
            JobStatus       status;
            unsigned int    progress; // [0..100]
        } jobStatus;
    };
    
} CUPSReturn;



#endif
