#ifndef M1_CUPS_PRINTER_H
#define M1_CUPS_PRINTER_H

#include "joblist.h"
#include "../monitor/prodcons.h"
#include "../printer/printer.h"


/**
 * Arguments for a CUPS printer (a printer client).
 */
typedef struct PrinterCliArgs {
    Printer*        p;
    ProdCons*       ordsQueue;  /** orders queue */
    ProdCons*       jobsQueue;  /** Job queue */
    
    CUPSJobList*    joblist;
} PrinterCliArgs;


/**
 * CUPS Printer thread.
 * 
 * WARNING: There is a difference between a printer (the physical object) represented in
 * ../printer/printer.h and a printer thread. The printer thread is a particular thread in
 * the CUPS server intended to communicate with a specific printer and manage its backend.
 * 
 * Such a thread will wait for high-priority orders or preprocessed jobs. Synchronized with
 * conditions.
 * 
 * WARNING: This will in fact spawn _2_ threads if the connection is wired.
 * 
 * @param vargs Pointer to a PrinterCliArgs
 */
void* cupsPrinterCli(void* vargs);


#endif
