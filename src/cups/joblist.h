#ifndef M1_CUPS_JOBLIST_H
#define M1_CUPS_JOBLIST_H

#include "job.h"
#include "../structures/list.h"


/**
 * Lists all the active or pending jobs.
 * 
 * Useful when a component needs to retrieve a job's printer to pass orders.
 * It assumes that there won't be more than UINT_MAX (4,294,967,295) jobs at one execution...
 */
typedef struct CUPSJobList {
    List*           list;
    unsigned int    nextID;
} CUPSJobList;


/**
 * Initialize a job list.
 */
int cupsCreateJobList(CUPSJobList* l);


/**
 * Destroy a job list.
 */
void cupsCleanJobList(CUPSJobList* l);


/**
 * Get an new ID. Increment the internal counter.
 */
unsigned int cupsJobNextID(CUPSJobList* l);


/**
 * Get current ID.
 */
unsigned int cupsJobCurrentID(CUPSJobList* l);


/**
 * Get the list position of a job with ID id.
 */
ListElement* cupsJobFindByID(CUPSJobList* l, unsigned int id);


/**
 * Add a job
 */
CUPSJob* cupsAddJob(CUPSJobList* l, CUPSJob* j);


/**
 * Remove the job designated by e.
 */
void cupsRemoveJob(CUPSJobList* l, ListElement* e);


#endif
