#include "filter.h"
#include "job.h"
#include "request.h"
#include "../communication/communication.h"

#include <stdio.h>
#include <unistd.h>


/**
 * Filter thread. Simulates a conversion from .xxx to .ps with a timer.
 */
void* cupsFilter(void* vargs)
{
    FilterArgs* args = (FilterArgs*) vargs;


    const char*  mimeName[MIME_COUNT] = {"odt", "pdf", "txt", "png"};
    unsigned int mimeTime[MIME_COUNT] = { 25,    10,    1,     4};

    CUPSJob  job    = {0, 0, MIME_TXT, 0, false, JSTAT_PENDING};

    while (job.type < MIME_COUNT)
    {
        // Get an unprocessed job from the queue
        prodconsGet(args->jobsQueue, &job);
        
        printf("[filter] Just got a job (printer: %d, type: %s, copy: %d)\n",
               job.printer, mimeName[job.type], job.nbCopies);

        // Sleep a specific amount of time, representing the conversion to postscript
        printf("[filter] Processing...\n");
        sleep(mimeTime[job.type]);
        printf("[filter] Processed !\n");


        // Push and get reference
        int comm = job.id;
        CUPSJob* jobptr = cupsAddJob(args->joblist, &job);
        printf("[filter] Assigning id %d\n", job.id);


        // Transmit the job reference to corresponding printer
        prodconsPush(&args->jobsReady[job.printer-1], &jobptr);
        printf("[filter] Pushed job %d\n", job.id);


        // Notify the client
        CUPSReturn ret;
        ret.submitted.id = job.id;
        envoyerOctets(comm, &ret, sizeof(CUPSReturn));
        cloreCommunication(comm);
    }

    return NULL;
}
