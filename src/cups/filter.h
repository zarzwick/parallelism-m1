#ifndef M1_CUPS_FILTER_H
#define M1_CUPS_FILTER_H

#include "joblist.h"
#include "../monitor/prodcons.h"


/**
 * Arguments for a filter.
 */
typedef struct FilterArgs {
    unsigned int    id;         /** ID */
    ProdCons*       jobsQueue;  /** Raw jobs queue */
    ProdCons*       jobsReady;  /** Processed jobs queue */
    
    CUPSJobList*    joblist;    
} FilterArgs;


/**
 * CUPS Filter thread.
 * 
 * Such a thread will wait for task to show up in the Jobs queue.
 * They are synchronized using a condition.
 * 
 * @param vargs Pointer to a FilterArgs
 */
void* cupsFilter(void* vargs);


#endif
