#include "joblist.h"


int cupsCreateJobList(CUPSJobList* l)
{
    l->nextID = 0;
    l->list = listCreate(sizeof(CUPSJob));
    return (l->list != NULL) ? 0 : -1;
}


void cupsCleanJobList(CUPSJobList* l)
{
    listFree(&l->list);
}


unsigned int cupsJobNextID(CUPSJobList* l)
{
    return (++ l->nextID);
}


unsigned int cupsJobCurrentID(CUPSJobList* l)
{
    return l->nextID;
}


static bool jobCompare(void* a, void* b)
{
    CUPSJob* A = (CUPSJob*) a;
    CUPSJob* B = (CUPSJob*) b;
    
    return A->id == B->id;
}


ListElement* cupsJobFindByID(CUPSJobList* l, unsigned int id)
{
    CUPSJob temp = {id, 0, MIME_TXT, 0, false, JSTAT_PENDING};
    return listFind(l->list, &temp, jobCompare);
}


CUPSJob* cupsAddJob(CUPSJobList* l, CUPSJob* j)
{
    j->id = (++l->nextID);
    ListElement* e = listPush(l->list, j);
    return listElementAt(e);
}


void cupsRemoveJob(CUPSJobList* l, ListElement* e)
{
    listRemove(l->list, e);
}
