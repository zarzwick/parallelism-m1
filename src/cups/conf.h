#ifndef M1_CUPS_CONF
#define M1_CUPS_CONF


/**
 * Load the CUPS configuration from a .ini.
 * The default is ../res/cups.conf.
 */

#include "../printer/printer.h"


/*
 * Constants
 */
#define M1_CUPSSERV_NAMELEN 32
#define M1_CUPSSERV_DEFAULT_NAME "M1CUPS"
#define M1_CUPSSERV_CONF_PATH "../res"
#define M1_CUPSSERV_DEFAULT_FILTER_COUNT 4


/**
 * Structure representing the CUPS configuration
 */
typedef struct CUPSConf
{
    char            servname[M1_CUPSSERV_NAMELEN];

    Printer*        printers;
    unsigned int    printerCount;
    
    unsigned int    filterCount;
} CUPSConf;


/**
 * Load the configuration from @param filename in @param conf
 * Return the number of printers if OK and -1 otherwise
 */
int cupsLoadConf(const char* filename, CUPSConf* conf);


/**
 * Clean up configuration.
 */
void cupsCleanConf(CUPSConf* conf);


/**
 * Get ID associated with a printer.
 * @return 0 if none, > 0 if found.
 */
unsigned int cupsPrinterID(CUPSConf* conf, const char* printername);


#endif
