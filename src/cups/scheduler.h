#ifndef M1_CUPS_SCHEDULER_H
#define M1_CUPS_SCHEDULER_H

#include "conf.h"
#include "joblist.h"
#include "../monitor/prodcons.h"


/**
 * Scheduler arguments
 */
typedef struct SchedulerArgs {
    
    // Server
    int serv;
    
    // Configuration
    CUPSConf* conf;
    
    // Job list
    CUPSJobList* joblist;
    
    // Synchronization and communication
    ProdCons* jobsQueue;
    ProdCons* ordsQueue; /** Array [conf.printerCount] */

} SchedulerArgs;


/**
 * The CUPS scheduler thread.
 * 
 * This thread will act as a request server for synchronous requests from users,
 * and provide filters with some jobs, or directly give the printers threads some
 * high-priority orders like "status" or "cancel".
 * 
 * @param vargs Pointer to a SchedulerArgs
 */
void* cupsScheduler(void* vargs);


#endif
