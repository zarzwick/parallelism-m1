#ifndef M1_CUPS_SERVER
#define M1_CUPS_SERVER

#include "../communication/communication.h"


/**
 * Start cups server.
 * @return (>= 0): socket number, (< 0): error message
 */
int cupsStartServer(const char* name)
{
    return initialiserServeur(name);
}


/**
 * Shutdown cups server.
 */
void cupsCleanServer(int serv)
{
    arreterServeur(serv);
}


#endif
