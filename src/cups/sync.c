#include "sync.h"
#include "job.h"
#include "../monitor/prodcons.h"

#include <stdlib.h>


int cupsCreateSync(CUPSSync* sync, CUPSConf* conf)
{
    int err;
    
    // Create the job queue for filters
    err = prodconsCreate(&sync->rawJobs, sizeof(CUPSJob));
    
    if (err != 0)
        return -1;
    
    
    // Allocate job and orders queues for each printer
    sync->jobs   = NULL;
    sync->orders = NULL;
    
    if ((sync->jobs = malloc(conf->printerCount * sizeof(ProdCons))) == NULL)
        return -1;
    
    if ((sync->orders = malloc(conf->printerCount * sizeof(ProdCons))) == NULL)
        return -1;


    // Initialize them
    for (unsigned int p = 0; p < conf->printerCount; ++ p)
    {
        err = prodconsCreate(&sync->jobs[p], sizeof(CUPSJob*));
        
        if (err != 0)
            return -1;
        
        err = prodconsCreate(&sync->orders[p], sizeof(CUPSOrder));
        
        if (err != 0)
            return -1;
    }
    
    return 0;
}


void cupsCleanSync(CUPSSync* sync, CUPSConf* conf)
{
    prodconsClean(&sync->rawJobs);
   
    for (unsigned int p = 0; p < conf->printerCount; ++ p)
    {
        prodconsClean(&sync->jobs[p]);
        prodconsClean(&sync->orders[p]);
    }

    free(sync->jobs);
    free(sync->orders);
}
