#include "conf.h"
#include "../iniparser/iniparser.h"

#include <assert.h>
#include <string.h>
#include <stdio.h>


/**
 * Retrieve complete name of the configuration file.
 */
static inline void cupsFindConfFile(const char* filename, char* destname)
{
    char defaultName[] = "cups.conf";
    
    if (filename == NULL)
        filename = defaultName;
    
    sprintf(destname, "%s/%s", M1_CUPSSERV_CONF_PATH, filename);
}


/**
 * Copy an entry from the .ini to a Printer.
 * Return >= 0 if correct or or < 0 if a failure occured.
 */
static inline int cupsParsePrinter(CUPSConf* conf, dictionary* dict, int n)
{
    // Get the printer's name
    char sectionName[M1_PRINTER_NAMELEN];
    strncpy(sectionName, iniparser_getsecname(dict, n), M1_PRINTER_NAMELEN);
    
    strncpy(conf->printers[n].name, sectionName, M1_PRINTER_NAMELEN);

    // Get the connection type
    char        key[M1_PRINTER_NAMELEN+10];
    const char* defaultType = "USB";
    const char* connectionType;
    
    sprintf(key, "%s:connection", sectionName);
    connectionType = iniparser_getstring(dict, key, defaultType);

    // Check the key was actually defined
    if (connectionType == NULL)
        return -1;
    
    // Record corresponding connection type
    if (strncmp(connectionType, "USB", M1_PRINTER_CONNTYPE_REPRLEN) == 0)
        conf->printers[n].conntype = P_USB;
    
    else if (strncmp(connectionType, "Serial", M1_PRINTER_CONNTYPE_REPRLEN) == 0)
        conf->printers[n].conntype = P_SERIAL;
        
    else if (strncmp(connectionType, "Wifi", M1_PRINTER_CONNTYPE_REPRLEN) == 0)
        conf->printers[n].conntype = P_WIFI;   

    else if (strncmp(connectionType, "The Force", M1_PRINTER_CONNTYPE_REPRLEN) == 0)
        conf->printers[n].conntype = P_THE_FORCE;
   
    conf->printers[n].id = n + 1;
    
    return 0;
}


/**
 * Load the configuration from @param filename in @param conf .
 */
int cupsLoadConf(const char* filename, CUPSConf* conf)
{
    assert(conf != NULL);
    
    
    // Get the complete name
    char name[M1_CUPSSERV_NAMELEN];
    cupsFindConfFile(filename, name);
    
    
    // Create the dictionary
    dictionary* dict = NULL;
    if ((dict = iniparser_load(name)) == NULL)
        return -1;
    
    
    // Parse and fill the configuration
    int sectionCount = iniparser_getnsec(dict);
    
    conf->printerCount = sectionCount;
    if ((conf->printers = malloc(sectionCount * sizeof(Printer))) == NULL)
        return -1;
    
    for (int s = 0; s < sectionCount; ++ s)
    {
        // Parse the section s
        cupsParsePrinter(conf, dict, s);
    }
 
 
    // Destroy the dictionary
    iniparser_freedict(dict);
    
    return sectionCount;
}


/**
 * Cleanup
 */
void cupsCleanConf(CUPSConf* conf)
{
    assert(conf->printers != NULL);
    
    free(conf->printers);
}


/**
 * Simple utility to get a printer's ID
 */
unsigned int cupsPrinterID(CUPSConf* conf, const char* printername)
{
    // Loop over each printer and compare name
    for (unsigned int i = 0; i < conf->printerCount; ++ i)
    {
        if (strcmp(printername, conf->printers[i].name) == 0)
            return conf->printers[i].id;
    }

    return 0;
}

