#ifndef M1_CUPS_THREAD_POOL
#define M1_CUPS_THREAD_POOL

#include "conf.h"
#include "sync.h"
#include "filter.h"
#include "joblist.h"
#include "scheduler.h"
#include "printercli.h"

#include <pthread.h>


/**
 * A thread pool structure.
 * 
 * @note This is not exactly a thread pool, as not every threads are dedicated to the same thing.
 */
typedef struct CUPSThreadPool CUPSThreadPool;
struct CUPSThreadPool
{
    // Array of threads
    pthread_t* threads;

    // Pointers to specific groups (DO NOT FREE)
    pthread_t* schedulerThread;
    pthread_t* filtersThread;
    pthread_t* printersThread;

    // Config
    unsigned int filterCount;
    unsigned int printerCount;

    // Arguments
    SchedulerArgs   schedArgs;
    PrinterCliArgs* prntrCliArgs;
    FilterArgs*     fltrArgs;

    // Housekeeping
    unsigned int threadCount;
};


/**
 * Allocate the thread "pool" and manage arguments passed to the threads.
 * 
 * @return 0 if ok, -1 otherwise.
 */
int cupsInitThreads(CUPSConf* conf,
                    CUPSSync* sunc,
                    CUPSThreadPool* pool,
                    CUPSJobList* joblist,
                    int serv);


/**
 * Free things.
 * You should definitely call it. Yeah.
 */
void cupsCleanThreads(CUPSThreadPool* pool);


/**
 * Launch the threads.
 */
int cupsStartThreads(CUPSThreadPool* pool);


/**
 * Join all threads.
 */
int cupsJoinThreads(CUPSThreadPool* pool);


#endif
