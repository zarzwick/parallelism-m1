#include "scheduler.h"
#include "request.h"
#include "conf.h"
#include "../structures/list.h"
#include "../structures/sharedbuffer.h"
#include "../communication/communication.h"

#include <stdio.h>
#include <pthread.h>


CUPSJob   jobstop, *jobstopptr = &jobstop;
CUPSOrder ordstop;

extern bool running;

/**
 * Dispatch request to others components.
 */
void processRequest(SchedulerArgs* args, CUPSRequest* req, int comm)
{
    if (req->reqType == REQ_SUBMIT)
    {
        CUPSJob job = {
            comm, // comm takes the place of ID until the job is processed by filter
            cupsPrinterID(args->conf, req->submit.printerName),
            req->submit.mimeType,
            req->submit.nbCopies,
            req->submit.rectoVerso,
            JSTAT_PENDING
        };

        if (job.printer == 0)
        {
            fprintf(stderr, "[scheduler] No printer named %s\n", req->submit.printerName);
            return;
        }

        prodconsPush(args->jobsQueue, &job);
    }

    else if (req->reqType == REQ_CANCEL)
    {
        CUPSOrder ord = {ORD_CANCEL, req->cancel.id, comm};

        // Find the job with given ID
        ListElement* jobptr = cupsJobFindByID(args->joblist, req->cancel.id);
        CUPSJob*     job    = (CUPSJob*) listElementAt(jobptr);

        // Send order to associated printer
        prodconsPush(&args->ordsQueue[job->printer-1], &ord);
        cloreCommunication(comm);
    }

    else if (req->reqType == REQ_JOB_STATUS)
    {
        CUPSOrder ord = {ORD_JOB_STATUS, req->jobStatus.id, comm};
        
        // Find the job with given ID
        ListElement* jobptr = cupsJobFindByID(args->joblist, req->jobStatus.id);
        CUPSJob*     job    = (CUPSJob*) listElementAt(jobptr);
        
        // Send order to the good printer
        prodconsPush(&args->ordsQueue[job->printer-1], &ord);
    }

    else if (req->reqType == REQ_PRINTER_STATUS)
    {
        // Find printer with given name
        unsigned int printerID = cupsPrinterID(args->conf, req->printerStatus.printerName);
        if (printerID == 0)
        {
            fprintf(stderr, "[scheduler] No printer named %s\n", req->printerStatus.printerName);
            return;
        }
        
        CUPSOrder ord = {ORD_PRINTER_STATUS, printerID, comm};
        
        // Send order to the good printer
        prodconsPush(&args->ordsQueue[printerID-1], &ord);
    }

    else
    {
        printf("[scheduler] Unknown order\n");
    }
}


/**
 * Thread function representing the scheduler in the CUPS server.
 */
void* cupsScheduler(void* vargs)
{
    // Get arguments
    SchedulerArgs* args = (SchedulerArgs*) vargs;


    // And wait orders
    while (running)
    {
        int comm = accepterCommunication(args->serv);
        printf("[network] Communication accepted, enormous.\n");


        CUPSRequest req;
        int x;
        if ((x = recevoirOctets(comm, &req, sizeof(CUPSRequest))) < (int) sizeof(CUPSRequest))
        {
            fprintf(stderr, "[network] Reception error (%db received)\n", x);
        }
        else
        {
            processRequest(args, &req, comm);
        }
    }


    // Send stop order to printers and filters
    jobstop.type = MIME_STOP;
    ordstop.ord  = ORD_STOP;
    
    for (unsigned int p = 0; p < args->conf->printerCount; ++ p)
        prodconsPush(&args->ordsQueue[p], &jobstopptr);
    
    for (unsigned int f = 0; f < args->conf->filterCount; ++ f)
        prodconsPush(&args->jobsQueue[f], &ordstop);

    return NULL;
}

