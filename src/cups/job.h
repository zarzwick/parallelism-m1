#ifndef M1_CUPS_JOB_H
#define M1_CUPS_JOB_H

#include <stdbool.h>


/**
 * File mime types.
 */
typedef enum MimeType {
    MIME_ODT = 0,
    MIME_PDF,
    MIME_TXT,
    MIME_PNG,
    MIME_COUNT,
    MIME_STOP       // Special mime type to signal termination
} MimeType;


/**
 * Job status.
 */
typedef enum JobStatus {
    JSTAT_PENDING = 0,
    JSTAT_PROCESSING,
    JSTAT_PROCESSED,
    JSTAT_COUNT
} JobStatus;


/**
 * Orders types.
 */
typedef enum Order {
    ORD_CANCEL = 0,
    ORD_JOB_STATUS,
    ORD_PRINTER_STATUS,
    ORD_COUNT,
    ORD_STOP        // Special order to signal termination
} Order;


/**
 * Cups job.
 */
typedef struct CUPSJob {
    unsigned int    id;
    unsigned int    printer;
    
    MimeType        type;
    unsigned int    nbCopies;
    bool            rectoVerso;
    
    JobStatus       status;
} CUPSJob;


/**
 * CUPS order.
 */
typedef struct CUPSOrder {
    Order           ord;
    unsigned int    id;     // Could be either a Printer ID or a Job ID
    int             client; // Client communication
} CUPSOrder;


#endif
