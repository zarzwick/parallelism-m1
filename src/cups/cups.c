/*
 * The Christmas Unix Printing System.
 */


#include "sync.h"
#include "conf.h"
#include "server.h"
#include "request.h"
#include "joblist.h"
#include "threadpool.h"

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>


bool            running = true;
int             serv;
CUPSConf        conf = {M1_CUPSSERV_DEFAULT_NAME, NULL, 0, M1_CUPSSERV_DEFAULT_FILTER_COUNT};
CUPSSync        sync;
CUPSThreadPool  tp;
CUPSJobList     joblist;


void interruptHandler(int);

    
int main(int argc, char* argv[])
{
    // The CUPS server basically works like this:
    //   . Load the configuration file
    //   . Initialize all the conditions and sync. mechanisms
    //   . Create the required thread pool (scheduler + filters + printers)
    //   . Wait for requests, jobs and high-priority orders

    
    // Argument checking
    if (argc > 4)
    {
        fprintf(stderr, "Usage: %s [-f CONFIG_FILE] [-n NB_FILTERS] [-s SERV_NAME]\n", argv[0]);
    }

    // Options
    char* configFile = NULL;
    
    char o;
    while ((o = getopt(argc, argv, "n:s:f:")) != -1)
    {
        switch (o)
        {
            case 'n': conf.filterCount = (unsigned int) strtoul(optarg, NULL, 10); break;
            case 's': sprintf(conf.servname, "%s", optarg); break;
            case 'f': configFile = optarg; break;
            default:  printf("AAARG\n");
        }
    }


    // Signal handling
    struct sigaction siga;
    siga.sa_handler = interruptHandler;
    siga.sa_flags   = SA_RESTART;
    sigemptyset(&siga.sa_mask);
    sigaction(SIGINT, &siga, NULL);
    

    // Load the configuration
    if (cupsLoadConf(configFile, &conf) < 0)
    {
        fprintf(stderr, "[error] Failed to load config. file %s\n",
                (configFile ? configFile : "../res/cups.conf"));
        return 1;
    }


    printf("[status] %u thread(s) allocated for filter(s)\n", conf.filterCount);
    printf("[status] Config ok, found %d printer(s)\n", conf.printerCount);
    printf("[status] Server name is %s\n", conf.servname);


    // Start the server
    if ((serv = cupsStartServer(conf.servname)) < 0)
    {
        fprintf(stderr, "[error] Failed to start server: %s\n", messageErreur(serv));
        cupsCleanConf(&conf);
        return 1;
    }


    // Initialize the synchronization mechanisms
    if (cupsCreateSync(&sync, &conf) < 0)
    {
        fprintf(stderr, "[error] Failed to initialize synchronization mechanism\n");
        cupsCleanConf(&conf);
        cupsCleanServer(serv);
        return 1;
    }
    
    if (cupsCreateJobList(&joblist) != 0)
    {
        fprintf(stderr, "[error] Failet to initialize job list\n");
        cupsCleanJobList(&joblist);
        cupsCleanConf(&conf);
        cupsCleanServer(serv);
    }


    // Init threads
    if (cupsInitThreads(&conf, &sync, &tp, &joblist, serv) < 0)
    {
        fprintf(stderr, "[error] Failed to initialize the thread pool\n");
        cupsCleanSync(&sync, &conf);
        cupsCleanConf(&conf);
        cupsCleanServer(serv);
        return 1;
    }
    
    // Launch threads
    if (cupsStartThreads(&tp) < 0)
    {
        fprintf(stderr, "[error] Failed to start threads (which is bad)\n");
        cupsCleanSync(&sync, &conf);
        cupsCleanConf(&conf);
        cupsCleanThreads(&tp);
        cupsCleanServer(serv);
        return 1;
    }

    // And join'em
    cupsJoinThreads(&tp);
    printf("[log] Joined everybody\n");
    
    
    // Cleanup'n'exit
    cupsCleanSync(&sync, &conf);
    cupsCleanThreads(&tp);
    cupsCleanServer(serv);
    cupsCleanConf(&conf);
    
    return 0;
}


void interruptHandler(int sig)
{
    if (sig != SIGINT)
        return;

    running = false;
    
    // Kill all threads
    //pthread_kill(tp.)

    // Cleanup
    cupsCleanThreads(&tp);
    cupsCleanServer(serv);
    cupsCleanConf(&conf);
    cupsCleanSync(&sync, &conf);
    
    exit(3);
}

