#include "printercli.h"
#include "request.h"
#include "../monitor/prodcons.h"
#include "../printer/printertask.h"
#include "../communication/communication.h"

#include <stdio.h>
#include <pthread.h>


/**
 * Enables to adapt behavior to the connection.
 */
typedef struct CUPSBackend {
    void (*fetch)(PrinterControl* ctrl);
    void (*push)(const PrinterControl* const ctrl);
} CUPSBackend;


void processOrder(CUPSOrder* ord, PrinterControl* ctrl, CUPSBackend* backend);

void fetchWired(PrinterControl* ctrl);
void fetchWireless(PrinterControl* ctrl);

void pushWired(const PrinterControl* const ctrl);
void pushWireless(const PrinterControl* const ctrl);


sem_t start;


/**
 * Printer controller thread. Establish communication between printers and client.
 */
void* cupsPrinterCli(void* vargs)
{
    PrinterCliArgs* args = (PrinterCliArgs*) vargs;
    pthread_t thread;


    // Printer/Controller shared memory
    PrinterControl ctrl = {&start, NULL, 0, false, PSTAT_IDLE, args->p->name};


    // Functions used to fetch/push a PrintControl block
    CUPSBackend backend = {NULL, NULL};
    
    
    if (args->p->conntype < P_WIRED_CONN)
    {
        printf("[control] Starting wired controller of %s (id %d)\n", args->p->name, args->p->id);
        
        // Init the semaphore (unshared)
        sem_init(ctrl.print, 0, 0);
        
        // Select wired backend
        backend.fetch = fetchWired;
        backend.push  = pushWired;

        // A new thread must be spawned, representing the physical printer
        pthread_create(&thread, NULL, printerTask, &ctrl);
    }
    else
    {
        printf("[control] Starting remote controller of %s (id %d)\n", args->p->name, args->p->id);

        // Init the semaphore (shared between processes)
        sem_init(ctrl.print, 1, 0);
        
        // Select remote backend
        backend.fetch = fetchWireless;
        backend.push  = pushWireless;
    }


    CUPSJob*    job = NULL;
    CUPSOrder   ord = {ORD_CANCEL, 0, 0};


    // Wait for orders (high priority) or jobs (low priority)
    while (ord.ord < ORD_COUNT)
    {
        // Try orders, and otherwise jobs
        if (prodconsSize(args->ordsQueue) > 0)
        {
            // Then there is an order available, to execute first
            // So let's do:
            //  . get the order
            //  . update/push (cancel) or fetch (status) control data
            //  . unlock and relock start mutex

            processOrder(&ord, &ctrl, &backend);
        }

        else if (prodconsSize(args->jobsQueue) > 0)
        {
            // Then a job is available, though.
            // Steps are:
            //  . get the job
            //  . update local job
            //  . push the control structure
            //  . unlock and relock start mutex to trigger the printer thread

            printf("[control] Job directly available\n");

            prodconsGet(args->jobsQueue, &job);

            ctrl.job = job;

            backend.push(&ctrl);

            printf("[control] Unlock start\n");
            sem_post(ctrl.print);
        }

        else
        {
            // Wait for a job or a queue, alternatively.
            // Will check both at a 2-seconds interval.

            const unsigned int PRODCONS_TIMEOUT = 2;
            bool avail = false;

            printf("[control] Waiting job or order...\n");
            
            while (! avail)
            {
                //printf("[control] Order...\n");
                if (prodconsWaitN(args->ordsQueue, PRODCONS_TIMEOUT) == 0)
                {
                    printf("[control] Got an order !\n");
                    avail = true;
                    continue;
                }
                
                //printf("[control] Job...\n");
                if (prodconsWaitN(args->jobsQueue, PRODCONS_TIMEOUT) == 0)
                {
                    printf("[control] Got a job !\n");
                    avail = true;
                    continue;
                }
            }
        }
    }


    sem_destroy(ctrl.print);
    
    return NULL;
}


void processOrder(CUPSOrder* ord, PrinterControl* ctrl, CUPSBackend* backend)
{
    CUPSReturn ret;
    
    if (ord->ord == ORD_CANCEL)
    {
        ctrl->abort = true;
        backend->push(ctrl);
    }

    else if (ord->ord == ORD_PRINTER_STATUS)
    {
        backend->fetch(ctrl);
        
        // Sync ?
        
        ret.printerStatus.status        = ctrl->status;
        ret.printerStatus.currentJobID  = ctrl->job->id;
        envoyerOctets(ord->client, &ret, sizeof(CUPSReturn));
    }

    else if (ord->ord == ORD_JOB_STATUS)
    {
        backend->fetch(ctrl);
        
        // Sync ?
        
        ret.jobStatus.progress = ctrl->progress;
        ret.jobStatus.status   = ctrl->job->status;
        envoyerOctets(ord->client, &ret, sizeof(CUPSReturn));
    }

    cloreCommunication(ord->client);
}


void fetchWired(PrinterControl* ctrl)
{
    if (ctrl->status == PSTAT_BUG)
    {
        fprintf(stderr, "[printercli] Printer is in a buggy state\n");
        return;
    }
}


void pushWired(const PrinterControl *const ctrl)
{
    if (ctrl->status == PSTAT_BUG)
    {
        fprintf(stderr, "[printercli] Printer is in a buggy state\n");
        return;
    }
}



void fetchWireless(PrinterControl* ctrl)
{
    if (ctrl->status == PSTAT_BUG)
    {
        fprintf(stderr, "[printercli] Printer is in a buggy state\n");
        return;
    }
    
    // Not sure, but I think you must send something to someone

}


void pushWireless(const PrinterControl *const ctrl)
{
    if (ctrl->status == PSTAT_BUG)
    {
        fprintf(stderr, "[printercli] Printer is in a buggy state\n");
        return;
    }

    // Not sure, but I think you must send something to someone
    
}

