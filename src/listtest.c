#include "structures/list.h"
#include <stdio.h>

int main()
{
    List* l = listCreate(sizeof(int));
    
    for (int i = 0; i < 5; ++ i)
    {
        ListElement* e = listPush(l, &i);
        int* ptr = listElementAt(e);
        printf("[lol] %d\n", *ptr);
    }

    for (int* x ; ! listEmpty(l); )
    {
        x = listTop(l);
        printf("[pop] %d\n", *x);
        listPop(l);
    }
}
