#ifndef M1_SHARED_BUFFER_H
#define M1_SHARED_BUFFER_H

#define M1_RINGBUFFER_SIZE 16

#include <stddef.h>
#include <stdbool.h>


/**
 * Generic ring buffer with monitor, based on a Queue provided with queue.h
 * Details of the monitors spec are in the documentation.
 */
typedef struct SharedBuffer SharedBuffer;


/**
 * Create a buffer.
 */
SharedBuffer* shbufferCreate(size_t typesize);


/**
 * Delete a buffer.
 */
void shbufferFree(SharedBuffer** shbuf);


/**
 * Is the buffer empty ?
 */
bool shbufferEmpty(SharedBuffer* shbuf);


/**
 * Is the buffer full ?
 */
bool shbufferFull(SharedBuffer* shbuf);


/**
 * Number of valid elements.
 */
unsigned int shbufferSize(SharedBuffer* shbuf);


/**
 * Add an element to the buffer.
 */
void shbufferPush(SharedBuffer* shbuf, const void* e);


/**
 * Remove the oldest element in the buffer and return it.
 * @param e Allocated mem. area to copy the value to.
 */
void* shbufferPull(SharedBuffer* shbuf, void* e);


#endif
