#ifndef M1_QUEUE_H
#define M1_QUEUE_H

#include <stddef.h>
#include <stdbool.h>


/**
 * Generic list implemented with good old void*.
 * Provides the following:
 */
typedef struct List List;


/**
 * Pointer-like type giving the position in the list.
 */
typedef struct Node ListElement;


/**
 * Create a List "Object"
 * @param typesize Enable object storage
 */
List* listCreate(size_t typesize);


/**
 * Delete a List
 */
void listFree(List** q);


/**
 * Add an element at back of the list
 * @param e Pointer to an element to insert
 */
ListElement* listPush(List* q, void* e);


/**
 * Remove the last element (physically) from the list
 */
void listPop(List* q);


/**
 * Remove any element (physically) from the list
 */
void listRemove(List* q, ListElement* e);


/**
 * Find an element in the list
 */
ListElement* listFind(List* q, void* e, bool (*cmp)(void* a, void* b));


/**
 * Access the top without removing it
 */
void* listTop(List* q);


/**
 * Returns wether or not list is empty
 */
bool listEmpty(List* q);


/**
 * Return list size
 */
int listSize(List* q);


/**
 * Return an element's position from the list.
 */
ListElement* listAt(List* q, int i);


/**
 * Return an element from it's position
 */
void* listElementAt(ListElement* e);


#endif
