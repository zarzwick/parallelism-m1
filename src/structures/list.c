#include "list.h"

#include <stdlib.h>
#include <string.h>

typedef struct Node Node;


/**
 * Internal node
 */
struct Node
{
    Node* next;
    Node* prev;
    void* data;
    char* storage;
};


/**
 * Actual structure for the List type
 */
struct List
{
    unsigned int size;
    size_t       tsize;
    
    Node front;
    Node back;
};


/**
 * Create the list
 */
List* listCreate(size_t typesize)
{
    List* q = malloc(sizeof(List));

    q->front.next = NULL;
    q->front.prev = &q->back;
    q->back.next  = &q->front;
    q->back.prev  = NULL;
    q->tsize = typesize;
    q->size  = 0;

    return q;
}


/**
 * Delete List
 */
void listFree(List** q)
{
    if (*q != NULL)
        free(q);
}


/**
 * Push element
 */
ListElement* listPush(List* q, void* e)
{
    Node* node = malloc(2*sizeof(Node*) + sizeof(void*) + q->tsize);

    node->next = q->back.next;
    node->next->prev = node;
    node->prev = &q->back;
    q->back.next = node;

    node->data = &(node->storage);
    memcpy(node->data, e, q->tsize);

    ++ q->size;

    return node;
}


/**
 * Pop element
 */
void listPop(List* q)
{
    if (listEmpty(q))
        return;
    
    Node* popop = q->front.prev;
    popop->prev->next = popop->next;
    popop->next->prev = popop->prev;
    
    -- q->size;
    
    free(popop);
}


/**
 * Suppress element
 */
void listRemove(List* q, ListElement* e)
{
    if (listEmpty(q))
        return;

    e->next->prev = e->prev;
    e->prev->next = e->next;
    
    -- q->size;
    
    free(e);
}


/**
 * Find an element in the list
 */
ListElement* listFind(List* q, void* e, bool (*cmp)(void *, void *))
{
    for (Node* n = q->front.prev; n != &q->back; n = n->prev)
    {
        if (cmp(n->data, e))
            return n;
    }
    
    return NULL;
}


/**
 * Find nth element in the list
 */
ListElement* listAt(List* q, int n)
{
    Node* node = q->front.prev;
    for (int i = 0; node != &q->back && i < n; node = node->prev);
    return node;
}


/**
 * Top element
 */
void* listTop(List* q)
{
    if (listEmpty(q))
        return NULL;
    else
        return q->front.prev->data;
}


/**
 * Element at position e
 */
void* listElementAt(ListElement* e)
{
    return e->data;
}



/**
 * Empty or not
 */
bool listEmpty(List* q)
{
    return (q->front.prev == &q->back);
}


/**
 * List size
 */
int listSize(List* q)
{
    return q->size;
}
