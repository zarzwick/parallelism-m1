#include "sharedbuffer.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>

/**
 * Implementation of a shared buffer synchronized with monitor, using a ring buffer.
 */
struct SharedBuffer {
    size_t          tsize;
    
    unsigned int    count;
    unsigned int    start;
    unsigned int    end;
    
    char*           data;
};


/**
 * Empty buffer ?
 */
bool shbufferEmpty(SharedBuffer* shbuf)
{
    assert(shbuf != NULL);
    return shbuf->count == 0;
}


/**
 * Full buffer ?
 */
bool shbufferFull(SharedBuffer* shbuf)
{
    assert(shbuf != NULL);
    return shbuf->count == M1_RINGBUFFER_SIZE;
}


/**
 * Number of elements.
 */
unsigned int shbufferSize(SharedBuffer* shbuf)
{
    return shbuf->count;
}


/**
 * Create a shared buffer with elements of size typesize.
 */
SharedBuffer* shbufferCreate(size_t typesize)
{
    SharedBuffer* buf = NULL;
    buf = malloc(sizeof(SharedBuffer));

    assert(buf != NULL);
    
    buf->tsize = typesize;    
    buf->count = 0;
    buf->start = 0;
    buf->end   = 0;
    
    buf->data = NULL;
    buf->data = malloc(M1_RINGBUFFER_SIZE * buf->tsize);
    
    assert(buf->data != NULL);
    
    return buf;
}


/**
 * Free the structure.
 */
void shbufferFree(SharedBuffer** shbuf)
{
    assert(shbuf != NULL);
    free((*shbuf)->data);
    free(*shbuf);
}



/**
 * Push a value on the queue.
 */
void shbufferPush(SharedBuffer* shbuf, const void* e)
{
    assert(shbuf != NULL);
    assert(! shbufferFull(shbuf));
    
    char* place = shbuf->data + (shbuf->tsize * shbuf->end);
    memcpy(place, e, shbuf->tsize);
    
    shbuf->end = (shbuf->end + 1) % M1_RINGBUFFER_SIZE;
    shbuf->count += 1;
}


/**
 * Get a value from the queue and discard.
 */
void* shbufferPull(SharedBuffer* shbuf, void* e)
{
    assert(shbuf != NULL);
    assert(! shbufferEmpty(shbuf));
    
    char* place = shbuf->data + (shbuf->tsize * shbuf->start);
    memcpy(e, place, shbuf->tsize);
    
    shbuf->start = (shbuf->start + 1) % M1_RINGBUFFER_SIZE;
    shbuf->count -= 1;
    
    return e;
}
