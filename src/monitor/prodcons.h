#ifndef M1_PROD_CONS_H
#define M1_PROD_CONS_H

#include "../structures/sharedbuffer.h"
#include <pthread.h>


/**
 * A structure defining the monitor Producer/Consumer implementing the following spec:
 * 
 * A producer can push an element of any type if a place is free, and will sleep otherwise against
 * the "prod" condition. He will be signaled by any consumer taking an element.
 * 
 * A consumer can pull (get) an element of any type if at least one element is present in the
 * buffer, and sleep against the "cons" condition otherwise. He will be signaled by any producer
 * producing an element.
 */
typedef struct ProdCons {

    pthread_mutex_t mutex;
    
    pthread_cond_t  prod;
    pthread_cond_t  cons;
    
    SharedBuffer*   buff;

} ProdCons;


/**
 * Initialize a monitor.
 */
int prodconsCreate(ProdCons* pc, size_t typesize);


/**
 * Clean a monitor.
 */
void prodconsClean(ProdCons* pc);


/**
 * Push an element on the monitor.
 */
int prodconsPush(ProdCons* pc, const void* e);


/**
 * Pull an element on the monitor.
 */
int prodconsGet(ProdCons* pc, void* e);


/**
 * Get the number of elements in the buffer.
 */
unsigned int prodconsSize(ProdCons* pc);


/**
 * Wait n seconds for the cons condition.
 * This function enables a consumer to wait a given time for the structure condition
 * without specifically get the element.
 * 
 * This may be useful if someone deals with multiples ProdCons and want to be able to
 * wait for one t seconds and check another ProdCons.
 * 
 * @param pc ProdCons structure
 * @param sec Seconds to wait
 * 
 * @return 0: GO, 1: NO GO
 */
int prodconsWaitN(ProdCons* pc, unsigned int sec);


#endif
