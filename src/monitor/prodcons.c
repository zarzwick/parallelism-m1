#include "prodcons.h"

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>


int prodconsCreate(ProdCons* pc, size_t typesize)
{
    pc->buff = shbufferCreate(typesize);
    
    if (pthread_mutex_init(&pc->mutex, 0) != 0)
    {
        fprintf(stderr, "[pc] Failed to init. mutex\n");
        return -2;
    }
    
    if (pthread_cond_init(&pc->prod, 0) != 0)
    {
        fprintf(stderr, "[pc] Failed to init. condition 'prod'\n");
        return -2;
    }
     
    if (pthread_cond_init(&pc->cons, 0) != 0)
    {
        fprintf(stderr, "[pc] Failed to init. condition 'cons'\n");
        return -2;
    }

    return (pc->buff == NULL) ? -1 : 0;
}


void prodconsClean(ProdCons* pc)
{
    shbufferFree(&pc->buff);
    pthread_mutex_destroy(&pc->mutex);
    pthread_cond_destroy(&pc->prod);
    pthread_cond_destroy(&pc->cons);
}


int prodconsPush(ProdCons* pc, const void* e)
{
    assert(pc != NULL);
    assert(e  != NULL);


    // Lock the mutex
    if (pthread_mutex_lock(&pc->mutex) != 0)
    {
        fprintf(stderr, "[prodcons] Locking mutex during push() failed\n");
        return -1;
    }

    
    // Wait for the condition
    while (shbufferFull(pc->buff))
    {
        if (pthread_cond_wait(&pc->prod, &pc->mutex) != 0)
        {
            fprintf(stderr, "[prodcons] Waiting condition prod failed\n");
            return -1;
        }
    }
    
    
    // Push
    shbufferPush(pc->buff, e);
    

    // Signal consumers
    pthread_cond_signal(&pc->cons);
    
    
    // Unlock mutex
    if (pthread_mutex_unlock(&pc->mutex) != 0)
        fprintf(stderr, "[prodcons] Unlocking mutex during push() failed\n");
    
    return 0;
}


int prodconsGet(ProdCons* pc, void* e)
{
    assert(pc != NULL);
    assert(e  != NULL);
    

    // Lock the mutex
    if (pthread_mutex_lock(&pc->mutex) != 0)
    {
        fprintf(stderr, "[prodcons] Locking mutex during get() failed\n");
        return -1;
    }


    // Wait for the condition
    while (shbufferEmpty(pc->buff))
    {
        if (pthread_cond_wait(&pc->cons, &pc->mutex) != 0)
        {
            fprintf(stderr, "[pc] Waiting condition cons failed\n");
            return -1;
        }
    }
    
    
    // Get
    shbufferPull(pc->buff, e);


    // Signal producers
    pthread_cond_signal(&pc->prod);


    // Unlock mutex
    if (pthread_mutex_unlock(&pc->mutex) != 0)
        fprintf(stderr, "[prodcons] Unlocking mutex during get() failed\n");
    
    
    return 0;
}


unsigned int prodconsSize(ProdCons* pc)
{
    return shbufferSize(pc->buff);
}


int prodconsWaitN(ProdCons* pc, unsigned int sec)
{
    assert(pc != NULL);
    

    // Lock the mutex
    if (pthread_mutex_lock(&pc->mutex) != 0)
    {
        fprintf(stderr, "[prodcons] Locking mutex during get() failed\n");
        return -1;
    }


    // Get absolute unprecise (but fast) time from the Epoch and add sec seconds
    struct timespec tspec;
    clock_gettime(CLOCK_REALTIME_COARSE, &tspec);

    tspec.tv_sec += sec;
    int err;


    // Wait for the specified time
    err = pthread_cond_timedwait(&pc->cons, &pc->mutex, &tspec);


    // Unlock mutex
    if (pthread_mutex_unlock(&pc->mutex) != 0)
        fprintf(stderr, "[prodcons] Unlocking mutex during get() failed\n");


    return (err == 0) ? 0 : 1;
}
