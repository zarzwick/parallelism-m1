#include "client.h"
#include "../cups/conf.h"
#include "../cups/request.h"
#include "../structures/list.h"
#include "../communication/communication.h"

#include <time.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>

const char* mimeName[MIME_COUNT]     = {"odt", "pdf", "txt", "png"};
const char* pstatString[PSTAT_COUNT] = {"idle", "busy", "buggy"};
const char* jstatString[JSTAT_COUNT] = {"pending", "processing", "processed"};

void strlow(char* string);


int main(int argc, char* argv[])
{
    // A client behaves as follows:
    //   . Get the CUPS server name
    //   . Spawn a given number of threads which emulate processes in an OS
    //   . Each thread can, at a random moment, send a random req. to CUPS

    
    // Argument checking
    if (argc < 3)
    {
        fprintf(stderr, "Usage: %s CUPS_SERV_NAME PRINTER1 [PRINTER2 [PRINTER3 ...]]\n", argv[0]);
        return 1;
    }
    
    const char* servName = argv[1];
    
    printf("[check] Will ping the server %s\n", servName);
    
    
    // Lowercase all names
    for (int i = 0; i < argc - 2; ++ i)
        strlow(argv[2+i]);


    // Init. the random generator
    srand(time(NULL));
    
    
    // Spawn a random number of threads
    // ...

    
    // Test thread                 v
    ClientArgs arg = {servName, 1, 1, &argv[2], argc - 2};
    pthread_t t;
    pthread_create(&t, NULL, clientTask, &arg);
    
    pthread_join(t, NULL);
    
    return 0;
}


void* clientTask(void* vargs)
{
    // A task (conceptually a processus, or an activity) randomly tries to communicate a
    // request with the server whose name is passed as argument.
    //
    // The task is given a lifetime and return by itself when done.

    ClientArgs* args = (ClientArgs*) vargs;
    
    List* joblist = listCreate(sizeof(unsigned int));
   
    // The lifetime is counted in number of request
    int lifeTime = args->lifeTime;

    while (lifeTime-- > 0)
    {
        // Connect to the server
        int comm = demanderCommunication(args->server);

        if (comm < 0)
        {
            fprintf(stderr, "[error] No communication given\n");
            continue;
        }

        CUPSRequest req;
        CUPSReturn  ret;
        
        // Generate an order
        //req.reqType = rand() % REQ_COUNT;
        req.reqType = REQ_SUBMIT;

        if (req.reqType == REQ_SUBMIT)
        {
            // Randomly create a job
            strcpy(req.submit.printerName, args->printers[rand() % args->printerCount]);
            req.submit.mimeType   = rand() % MIME_COUNT;
            req.submit.nbCopies   =(rand() % 9) + 1;
            req.submit.rectoVerso = rand() % 2;

            printf("[client-%d] Submitting %s (%d copies%s) to %s\n",
                   args->id,
                   mimeName[req.submit.mimeType],
                   req.submit.nbCopies,
                   (req.submit.rectoVerso) ? ", recto-verso" : "",
                   req.submit.printerName
                 );

            // Send
            envoyerOctets(comm, &req, sizeof(CUPSRequest));

            // Receive job ID
            recevoirOctets(comm, &ret, sizeof(CUPSReturn));

            printf("[client-%d] Received ID %d\n", args->id, ret.submitted.id);

            // Remember this ID for later cancellation or status checking or whatever
            listPush(joblist, &ret.submitted.id);
        }

        else if (req.reqType == REQ_CANCEL)
        {
            if (listEmpty(joblist))
                continue;
            
            int jobIndex  = rand() % listSize(joblist);
            CUPSJob* job  = listElementAt(listAt(joblist, jobIndex));
            req.cancel.id = job->id;
            
            printf("[client-%d] Requesting cancellation of job %d\n",
                   args->id,
                   req.cancel.id
                );
            
            envoyerOctets(comm, &req, sizeof(CUPSRequest));
        }

        else if (req.reqType == REQ_JOB_STATUS)
        {
            if (listEmpty(joblist))
                continue;
            
            int jobIndex  = rand() % listSize(joblist);
            CUPSJob* job  = listElementAt(listAt(joblist, jobIndex));
            req.cancel.id = job->id;
            
            printf("[client-%d] Requesting status of job %d\n",
                   args->id,
                   req.cancel.id
                );
            
            envoyerOctets(comm, &req, sizeof(CUPSRequest));
            
            recevoirOctets(comm, &ret, sizeof(CUPSReturn));
            
            printf("[client-%d] Job %d is in state: %s (%d%%)\n",
                   args->id,
                   req.jobStatus.id,
                   jstatString[ret.jobStatus.status],
                   ret.jobStatus.progress
                );
        }
        
        else if (req.reqType == REQ_PRINTER_STATUS)
        {
            strcpy(req.printerStatus.printerName, args->printers[rand() % args->printerCount]);
            
            printf("[client-%d] Requesting %s status...\n",
                   args->id, req.printerStatus.printerName);

            envoyerOctets(comm, &req, sizeof(CUPSRequest));
            
            recevoirOctets(comm, &ret, sizeof(CUPSReturn));
            
            printf("[client-%d] %s is in state: %s\n",
                   args->id,
                   req.printerStatus.printerName,
                   pstatString[ret.printerStatus.status]
                  );
        }

        cloreCommunication(comm);
    }
    
    return NULL;
}


void strlow(char* string)
{
    for (char* c = string; *c != '\0'; ++ c)
        *c = tolower(*c);
}
