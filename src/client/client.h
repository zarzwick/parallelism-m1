#ifndef M1_CLIENT
#define M1_CLIENT

/**
 * Useful structures for a client.
 */


/**
 * A client thread (conceptually a process).
 * The thread will get a lifetime.
 */
typedef struct ClientArgs {
    const char*     server;
    unsigned int    id;
    unsigned int    lifeTime;
    
    char**          printers;
    unsigned int    printerCount;
    
} ClientArgs;


/**
 * A client task (or activity, or process).
 * @param vargs Pointer to a ClientArgs
 */
void* clientTask(void* vargs);


#endif
