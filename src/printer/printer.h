#ifndef M1_PRINTER_H
#define M1_PRINTER_H

#include "printertask.h"


/**
 * This file contains printers related definitions, like the connection type and so on...
 */


/*
 * Constants 
 */
#define M1_PRINTER_NAMELEN 32
#define M1_PRINTER_CONNTYPE_REPRLEN 16


/**
 * Printer connection type. Those before P_LOCAL_MARKER are locals, those after are remote.
 * This may also be referred to as CUPS backend.
 */
typedef enum PrinterConnection {
    P_USB = 0,
    P_SERIAL,
    P_WIRED_CONN,
    P_WIFI,
    P_THE_FORCE,                         // Use it, Luke.
    P_WIRELESS_CONN
} PrinterConnection;


typedef struct PrinterComm
{
    sem_t* print;
    //PrinterControl;
} PrinterComm;


/**
 * A printer type
 */
typedef struct Printer {
    char                name[M1_PRINTER_NAMELEN];
    PrinterConnection   conntype;
    unsigned int        id;
} Printer;


#endif
