#include "printertask.h"
#include "printer.h"

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>


/**
 * Defines the behaviour of a printer task, which could be on its own site-machine or
 * embedded on the CUPS host. This means the whole task must be a "thread function".
 * 
 * The behaviour is to repeatedly try to get the start mutex and launch printing, while checking
 * the abort boolean to stop on order.
 * 
 * Every document is assumed to be one page (to simplify) and a cancellation cancels only further
 * copies. The current copy will be totally printed.
 * 
 * @param vargs A pointer to a PrinterArgs
 */
void* printerTask(void* vargs)
{
    PrinterControl* ctrl = (PrinterControl*) vargs;

    unsigned int mimeTime[MIME_COUNT] = {14, 7, 4, 6};


    while (ctrl->abort == false)
    {
        // Lock the start mutex.
        if (sem_wait(ctrl->print) != 0)
        {
            fprintf(stderr, "[print] Failed to wait semaphore\n");
            continue;
        }

        printf("[%s] Start for %d copies.\n", ctrl->printerName, ctrl->job->nbCopies);
        ctrl->job->status = JSTAT_PROCESSING;

        // Print N copies
        for (unsigned int c = 1; (ctrl->abort == false) && (c <= ctrl->job->nbCopies); ++ c)
        {
            printf("[%s] Copy #%d of job %d...\n", ctrl->printerName, c, ctrl->job->id);
            sleep(mimeTime[ctrl->job->type]);
            ctrl->progress = (100/ctrl->job->nbCopies) * c;
        }

        ctrl->job->status = JSTAT_PROCESSED;
        printf("[%s] Stop.\n", ctrl->printerName);
    }

    return NULL;
}
