#ifndef M1_PRINTER_TASK
#define M1_PRINTER_TASK

#include "../cups/job.h"

#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>


/**
 * Printer status.
 */
typedef enum PrinterStatus {
    PSTAT_IDLE = 0,
    PSTAT_BUSY,
    PSTAT_BUG,
    PSTAT_COUNT
} PrinterStatus;


/**
 * Information block shared by a printer controller and its printer (the task).
 */
typedef struct PrinterControl {
    
    sem_t*          print;

    CUPSJob*        job;
    unsigned int    progress;
    bool            abort;
    
    PrinterStatus   status;
    
    const char*     printerName;

} PrinterControl;


/**
 * Defines the behaviour of a printer task, which could be on its own site-machine or
 * embedded on the CUPS host. This means the whole task must be a "thread function".
 * 
 * @param vargs A pointer to a PrinterArgs
 */
void* printerTask(void* vargs);


#endif
