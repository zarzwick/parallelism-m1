#include "printer.h"
#include "../cups/conf.h"
#include "../communication/communication.h"

#include <stdio.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>


void interruptHandler(int sig);


bool running = true;
int  serv = -1;

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s PRINTER_NAME\n", argv[0]);
        return 2;
    }
    
    
    // Generate server name
    char servname[M1_CUPSSERV_NAMELEN];
    sprintf(servname, "M1CUPS_%s", argv[1]);
    
    
    // Setup SIG_INTR handler
    struct sigaction siga;
    siga.sa_flags = SA_RESTART;
    siga.sa_handler = interruptHandler;
    sigemptyset(&siga.sa_mask);
    sigaction(SIGINT, &siga, NULL);
    
    
    // Launch the printer interface (a server)
    serv = initialiserServeur(servname);
    if (serv < 0)
    {
        fprintf(stderr, "[damned] Failed to start printer interface\n");
        return 1;
    }
    
    
    PrinterControl ctrl = {NULL, NULL, 0, false, PSTAT_IDLE, argv[1]};

    sem_init(ctrl.print, 0, 0);

    if (pthread_mutex_lock(ctrl.print ) != 0)
    {
        fprintf(stderr, "[mutex] Failed to lock the controller's mutex\n");
        return 1;
    }


    // Create physical printer thread
    pthread_t thread;
    if (pthread_create(&thread, NULL, printerTask, &ctrl) != 0)
    {
        fprintf(stderr, "[damned] Is the printer plugged ?\n");
        arreterServeur(serv);
        return 1;
    }
    
    
    int comm = accepterCommunication(serv);

    while (running)
    {
        recevoirOctets(comm, &ctrl, sizeof(PrinterControl));
        
        // Nearly the same code as in printercli
        
    }

    cloreCommunication(comm);
    
    
    // Clean up
    pthread_join(thread, NULL);
    pthread_mutex_destroy(ctrl.print );
    arreterServeur(serv);
    
    return 0;
}


void interruptHandler(int sig)
{
    if (sig != SIGINT)
        return;
 
    running = false;
}
