#! /bin/bash

## This script assume the given terminal (or konsole by default) understands the -e
## option to execute the command, like: konsole -e cups M1CUPS

cd bin


# Check ELFs
if [[ ! -e cups || ! -e client || ! -e printer ]]
then
    echo "Please build first !"
fi


# some cool variables
conf='../res/cups.conf'
serv='M1CUPS'
term=$1
if [[ -z $term ]]
then
    term=konsole
fi


# Create the list of printers
printers=$(cat ${conf} | egrep "^\[.*\]$" | tr -d '[]' | tr -s "\n" ' ')


# Select remote ones
remoteexpr='wifi|theforce'
co=$(cat ${conf} | egrep "^[Cc]onnection.?=.*")
remoteprinters=$(paste <(echo $printers) <(echo $co | tr -d ' ') | egrep -i "$remoteexpr" | cut -f1)


# Launch remote printers
for rp in $remoteprinters
do
    echo "$term -e printer $rp"
    #${term} -e printer $rp &
done


# Launch cups server
echo "${term} -e cups -f ${conf} -s ${serv}"
#${term} -e cups -f ${conf} -s ${serv} &


# Launch clients
for i in $(seq 5)
do
    echo "${term} -e client ${serv} ${printers}"
    #${term} -e client ${serv} ${printers} &
done


# Unlink in case the builtin mechanism failed
if [[ -e ${serv} ]]
    unlink ${serv}


exit 0
